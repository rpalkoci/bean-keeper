package com.bean_keeper.model;

import com.bean_keeper.Proto;

import java.io.Serializable;

/**
 * Created by Roman on 5/8/14.
 */

/**
 * Class for holding {@code Proto.Transaction} and
 * flag if the transaction is synchronized with server or
 * transaction is new or modified
 */
public class MyTransaction implements Comparable<MyTransaction>, Serializable{

    private Proto.Transaction transaction;
    /**
     * true if transaction is new or modified
     * false if transaction is synchronized with server
     */
    private boolean dirty;

    public MyTransaction(Proto.Transaction transaction, boolean dirty) {
        this.transaction = transaction;
        this.dirty = dirty;
    }

    public Proto.Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Proto.Transaction transaction) {
        this.transaction = transaction;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    @Override
    public int compareTo(MyTransaction myTransaction) {
        // For sorting transaction by date from newest to oldest
        return Long.valueOf(myTransaction.getTransaction().getDate()).compareTo(transaction.getDate());
    }
}

package com.bean_keeper.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bean_keeper.Proto;
import com.bean_keeper.R;
import com.bean_keeper.activities.AddOrEditTransactionActivity;
import com.bean_keeper.adapters.base.TransactionsAdapter;
import com.bean_keeper.model.MyTransaction;
import com.bean_keeper.utils.IOUtils;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman on 5/8/14.
 */
public class MainFragment extends Fragment {
    /**
     * Request code for adding new or editing transaction.
     */
    public static final int MAIN_REQUEST_CODE = 100;
    /**
     * Activity result: new transaction was added.
     */
    public static final int ADD_RESULT_CODE = 200;
    /**
     * Activity result: transaction was edited.
     */
    public static final int EDIT_RESULT_CODE = 202;
    /**
     * Activity result: transaction was deleted.
     */
    public static final int DELETE_RESULT_CODE = 203;
    /**
     * Key for last synchronization property in shared preferences.
     */
    private static final String PREF_LAST_SYNC = "long_last_sync";

    /**
     * Adapter for holding all transactions data.
     */
    private TransactionsAdapter adapter;

    /**
     * Text view for displaying current account balance
     */
    private TextView totalView;

    private ProgressBar progressBar;

    /**
     * Flag indicates if data in adapter has been changed (added, modified, deleted)
     *
     */
    private boolean isDataDirty;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        adapter = new TransactionsAdapter(getActivity());
        loadData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        progressBar = (ProgressBar) rootView.findViewById(R.id.fragment_main_progress_bar);
        totalView = (TextView) rootView.findViewById(R.id.fragment_main_total_balance);
        updateTotalView(adapter.getTotalBalance());
        ListView list = (ListView) rootView.findViewById(R.id.fragment_main_transactions_list);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), AddOrEditTransactionActivity.class);
                intent.putExtra(AddOrEditTransactionFragment.EDITED_TRANSACTION, (MyTransaction) adapter.getItem(i));
                intent.putExtra(AddOrEditTransactionFragment.EDITED_TRANSACTION_POSITION, i);
                startActivityForResult(intent, MAIN_REQUEST_CODE);
            }
        });
        return rootView;
    }

    /**
     * Loads transaction data from files in background and sets data in adapter
     */
    private void loadData() {
        new AsyncTask<Void, Void, List<MyTransaction>>() {

            @Override
            protected List<MyTransaction> doInBackground(Void... voids) {
                List<MyTransaction> result = new ArrayList<MyTransaction>();
                result.addAll(loadData(getResources().getString(R.string.clean), false));   // Read all clean transactions
                result.addAll(loadData(getResources().getString(R.string.dirty), true));    // Read all dirty transactions
                return result;
            }

            @Override
            protected void onPostExecute(List<MyTransaction> myTransactions) {
                super.onPostExecute(myTransactions);
                adapter.setData(myTransactions);
                if (totalView != null){
                    updateTotalView(adapter.getTotalBalance());
                }
            }
        }.execute();
    }

    /**
     * Loads stored transactions data from file
     *
     * @param fileName the name of file
     * @param dirty the flag of the transaction, true = dirty, false = clean
     * @return the list of {@code MyTransaction} with the given flag
     */
    private List<MyTransaction> loadData(String fileName, boolean dirty) {
        List<MyTransaction> result = new ArrayList<MyTransaction>();
        Proto.AccountDelta data = IOUtils.readDataFromFile(getActivity(), fileName);
        if (data != null && data.getAddedOrModifiedList() != null) {
            for (Proto.Transaction transaction : data.getAddedOrModifiedList()) {
                if (transaction.hasDeleted() && transaction.getDeleted()) {
                    continue;
                }
                result.add(new MyTransaction(transaction, dirty));
            }
        }
        return result;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_new:
                // Add new transaction
                Intent intent = new Intent(getActivity(), AddOrEditTransactionActivity.class);
                startActivityForResult(intent, MAIN_REQUEST_CODE);
                return true;
            case R.id.action_sync:
                // Check if there is connection to internet
                ConnectivityManager connMgr = (ConnectivityManager)
                        getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                if (networkInfo != null && networkInfo.isConnected()) {
                    new SyncTransactionTask().execute(
                            Proto.AccountDelta.newBuilder()
                                    .addAllAddedOrModified(adapter.getDataWithFlag(true)) // get data from adapter with dirty flag
                                    .setServerTimestamp(getLastSuccessfulSync(getActivity()))
                                    .build()
                    );
                } else {
                    showNoConnectionDialog(getActivity());
                }
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Shows {code AlertDialog}, that no internet connection is available, with positive button
     * that redirects user to system settings
     */
    private void showNoConnectionDialog(Context context) {
        final Context ctx = context;
        new AlertDialog.Builder(ctx)
                .setMessage(R.string.no_connection_message)
                .setTitle(R.string.no_connection_title)
                .setPositiveButton(R.string.settings, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ctx.startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
                        // TODO after network state changed run synchronization automatically, and change alert message (remove "and try again")
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MAIN_REQUEST_CODE) {
            isDataDirty = true;
            switch (resultCode) {
                case ADD_RESULT_CODE:
                    adapter.addItem((Proto.Transaction) data.getSerializableExtra(AddOrEditTransactionFragment.NEW_PROTO_TRANSACTION));
                    break;
                case EDIT_RESULT_CODE:
                    adapter.replaceAt(data.getIntExtra(AddOrEditTransactionFragment.EDITED_TRANSACTION_POSITION, -1),
                            (Proto.Transaction) data.getSerializableExtra(AddOrEditTransactionFragment.EDITED_TRANSACTION));
                    break;
                case DELETE_RESULT_CODE:
                    adapter.deleteItemAt(data.getIntExtra(AddOrEditTransactionFragment.EDITED_TRANSACTION_POSITION, -1));
                    break;
            }
            updateTotalView(adapter.getTotalBalance());
        }
    }

    private void updateTotalView(double totalBalance) {
        totalView.setText(String.valueOf(totalBalance));
    }

    @Override
    public void onPause() {
        super.onPause();
        // When user is leaving fragment and if data in adapter was modified, then stores it
        if (isDataDirty) {
            isDataDirty = false;
            saveData(getActivity());
        }
    }

    /**
     * Saves transactions from adapter in to 2 files, one file for dirty transaction, one for clean
     */
    private void saveData(Context context) {
        IOUtils.writeDataToFile(context, context.getResources().getString(R.string.dirty),
                Proto.AccountDelta.newBuilder()
                        .addAllAddedOrModified(adapter.getDataWithFlag(true))
                        .setServerTimestamp(getLastSuccessfulSync(context))
                        .build());
        IOUtils.writeDataToFile(context, context.getResources().getString(R.string.clean),
                Proto.AccountDelta.newBuilder()
                        .addAllAddedOrModified(adapter.getDataWithFlag(false))
                        .setServerTimestamp(getLastSuccessfulSync(context))
                        .build());
    }

    /**
     * This class perform the synchronization in background
     */
    private class SyncTransactionTask extends AsyncTask<Proto.AccountDelta, Void, Proto.AccountDelta> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Proto.AccountDelta doInBackground(Proto.AccountDelta... accountDeltas) {
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost method = new HttpPost("http://bean-keeper.appspot.com/bk");
            // TODO set header to "application/octet-stream" or application/x-protobuf ?
            method.setEntity(new ByteArrayEntity(accountDeltas[0].toByteArray()));
            try {
                HttpResponse response = client.execute(method);
                return Proto.AccountDelta.parseFrom(response.getEntity().getContent());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Proto.AccountDelta accountDelta) {
            super.onPostExecute(accountDelta);
            if (accountDelta != null) {
                adapter.updateData(accountDelta.getAddedOrModifiedList());
                updateTotalView(adapter.getTotalBalance());
                isDataDirty = true;
                setLastSuccessfulSync(getActivity(), accountDelta.getServerTimestamp());
            }
            progressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Gets the time stamp of last successful synchronization in milliseconds since 1970
     *
     * @return the time stamp of last successful synchronization
     */
    public long getLastSuccessfulSync(Context context) {
        return getPreferences(context).getLong(PREF_LAST_SYNC, 0);
    }

    /**
     * Sets the time stamp of last successful synchronization in milliseconds since 1970
     *
     * @param lastSuccessfulSync the time stamp of last successful synchronization
     */
    public void setLastSuccessfulSync(Context context, long lastSuccessfulSync) {
        getPreferences(context).edit().putLong(PREF_LAST_SYNC, lastSuccessfulSync).commit();
    }

    /**
     * @return application's {@code SharedPreferences}
     */
    private SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(MainFragment.class.getSimpleName(), Context.MODE_PRIVATE);
    }
}

package com.bean_keeper.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.bean_keeper.Proto;
import com.bean_keeper.R;
import com.bean_keeper.model.MyTransaction;

import java.util.UUID;

/**
 * Created by Roman on 5/8/14.
 */
public class AddOrEditTransactionFragment extends Fragment implements ActionBar.TabListener {

    /**
     * Key of current tab position saved/restored in/from savedInstanceState
     */
    private static final String CURRENT_TAB_INT = "current_selected_tab";
    /**
     * Key of current transaction value saved/restored in/from savedInstanceState
     */
    private static final String CURRENT_TRANSACTION_VALUE_STRING = "current_transaction_value";
    /**
     * Key of current transaction kind value saved/restored in/from
     */
    private static final String CURRENT_TRANSACTION_KIND_STRING = "current_transaction_kind";

    public static final int OUTCOME = 0;
    public static final int INCOME = 1;
    /**
     * Key of transaction type (INPUT / OUTPUT)
     */
    private static final String ARG_OPTION_INT = "option";
    /**
     * Key indicates if the fragment is editing transaction (true) or adding new transaction (false)
     */
    public static final String EDIT_MODE_BOOL = "mode";

    /**
     * Keys of data putted in extra data of intent. This intent is result dta that the activity send to its caller(startActivityForResult)
     */
    public static final String NEW_PROTO_TRANSACTION = "new_or_modified_transaction";
    public static final String EDITED_TRANSACTION = "edited_transaction";
    public static final String EDITED_TRANSACTION_POSITION = "edited_transaction_position";

    /**
     * Edit text that holds and edit kind of transaction
     */
    private EditText kindEditText;
    /**
     * Edit text that holds end edit transaction value
     */
    private EditText transactionEditText;
    /**
     * Values 1 or -1 depend of the type of transaction INCOME/OUTCOME
     */
    private double sign = 1;

    /**
     * @param position the position of the transaction in adapter
     * @param myTransaction the transaction to be edited or deleted
     * @return the new instance of {@code AddOrEditTransactionFragment}
     */
    public static AddOrEditTransactionFragment newInstance(int position, MyTransaction myTransaction) {
        AddOrEditTransactionFragment fragment = new AddOrEditTransactionFragment();
        Bundle bundle = new Bundle();
        if (myTransaction != null) {
            bundle.putBoolean(EDIT_MODE_BOOL, true);
            bundle.putInt(ARG_OPTION_INT, myTransaction.getTransaction().getValue() > 0 ? INCOME : OUTCOME);
            bundle.putSerializable(EDITED_TRANSACTION, myTransaction.getTransaction());
            bundle.putInt(EDITED_TRANSACTION_POSITION, position);
        }
        fragment.setArguments(bundle);
        return fragment;
    }

    public AddOrEditTransactionFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_or_edit, container, false);
        kindEditText = (EditText) rootView.findViewById(R.id.fragment_add_kind_edit_text);
        transactionEditText = (EditText) rootView.findViewById(R.id.fragment_add_transaction_edit_text);
        transactionEditText.addTextChangedListener(new TextWatcher() {
            public boolean deleting;

            @Override
            public void beforeTextChanged(CharSequence sequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence sequence, int i, int before, int count) {
                deleting = before > count;
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String s = editable.toString();
                if (s.startsWith("0") && !s.startsWith("0.") && !deleting) {
                    transactionEditText.setText(transactionEditText.getText().insert(1, "."));
                    Selection.setSelection(transactionEditText.getText(), transactionEditText.length());
                }
                if (s.startsWith(".")) {
                    transactionEditText.setText(transactionEditText.getText().insert(0, "0"));
                    Selection.setSelection(transactionEditText.getText(), 2);
                }
            }
        });

        // Set up the action bar.
        ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle(getArguments().getBoolean(EDIT_MODE_BOOL) ? R.string.title_activity_edit_transaction : R.string.title_activity_add_transaction);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        // Create a tab with text corresponding to the option (income/outcome)
        // Also specify this object, which implements
        // the TabListener interface, as the callback (listener) for when
        // this tab is selected and change text view appearance
        actionBar.addTab(
                actionBar.newTab()
                        .setText(R.string.outcome)
                        .setTabListener(this),
                OUTCOME);
        actionBar.addTab(
                actionBar.newTab()
                        .setText(R.string.income)
                        .setTabListener(this),
                INCOME
        );


        if (savedInstanceState != null) {
            // restore values from saved instance state
            actionBar.selectTab(actionBar.getTabAt(savedInstanceState.getInt(CURRENT_TAB_INT, 0)));
            transactionEditText.setText(savedInstanceState.getString(CURRENT_TRANSACTION_VALUE_STRING));
            kindEditText.setText(CURRENT_TRANSACTION_KIND_STRING);
        } else {
            // else sets values based on arguments
            actionBar.selectTab(actionBar.getTabAt(getArguments().getInt(ARG_OPTION_INT, 0)));
            if (getArguments().getBoolean(EDIT_MODE_BOOL)) {
                Proto.Transaction transaction = (Proto.Transaction) getArguments().getSerializable(EDITED_TRANSACTION);
                transactionEditText.setText(String.valueOf(Math.abs(transaction.getValue())));
                kindEditText.setText(transaction.getKind());
            }
        }
        return rootView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (getArguments().getBoolean(EDIT_MODE_BOOL)) {
            MenuItem item = menu.findItem(R.id.action_delete);
            if (item != null)
                item.setVisible(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_done:
                if (!isInputCorrect()) {
                    return true;
                }
                if (getArguments().getBoolean(EDIT_MODE_BOOL)) {
                    if (hasTransactionChanged()) {
                        saveAndFinish(MainFragment.EDIT_RESULT_CODE, EDITED_TRANSACTION);
                    } else {
                        getActivity().finish();
                    }
                } else {
                    saveAndFinish(MainFragment.ADD_RESULT_CODE, NEW_PROTO_TRANSACTION);
                }
                return true;
            case R.id.action_delete:
                new AlertDialog.Builder(getActivity())
                        .setTitle(getResources().getString(R.string.delete_alert_title))
                        .setMessage(getResources().getString(R.string.delete_alert_message))
                        .setPositiveButton(R.string.delete,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        deleteAndFinish(MainFragment.DELETE_RESULT_CODE);
                                        Toast.makeText(getActivity(), getResources().getString(R.string.delete_alert_successful), Toast.LENGTH_SHORT).show();
                                    }
                                })
                        .setNegativeButton(android.R.string.cancel,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                })
                        .show();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Sets result of activity and put position of deleted transaction in to extra data
     *
     * @param resultCode the result code of activity (MainFragment.DELETE_RESULT_CODE)
     */
    private void deleteAndFinish(int resultCode) {
        Intent intent = new Intent();
        intent.putExtra(EDITED_TRANSACTION_POSITION, getArguments().getInt(EDITED_TRANSACTION_POSITION));
        getActivity().setResult(resultCode, intent);
        getActivity().finish();
    }

    /**
     * Checks if editable part of transaction has been changed.
     *
     * @return true, if transaction has been changed, otherwise false
     */
    private boolean hasTransactionChanged() {
        Proto.Transaction editedTransaction = (Proto.Transaction) getArguments().getSerializable(EDITED_TRANSACTION);
        Double val = Double.valueOf(transactionEditText.getText().toString()) * sign;
        if (val != editedTransaction.getValue()) {
            return true;
        }
        if (!kindEditText.getText().toString().equals(editedTransaction.getKind())) {
            return true;
        }
        return false;
    }

    /**
     * Sets result of activity and data based on input(if its new)
     * position and data from edited transaction (if its modified)
     *
     * @param resultCode the result code of activity (MainFragment.ADD_RESULT_CODE or MainFragment.EDIT_RESULT_CODE)
     * @param name       the name of the extra data (NEW_PROTO_TRANSACTION or EDITED_TRANSACTION)
     */
    private void saveAndFinish(int resultCode, String name) {
        Proto.Transaction.Builder builder = Proto.Transaction.newBuilder();
        // if the transaction is edited
        Proto.Transaction editedTransaction = (Proto.Transaction) getArguments().getSerializable(EDITED_TRANSACTION);
        if (editedTransaction != null) {
            builder.setDate(editedTransaction.getDate()) // date when transaction was created
                    .setGuid(editedTransaction.getGuid()); // GUID has to stayed the same
        } else { // in case this is new transaction
            builder.setDate(System.currentTimeMillis()) // set date on current time
                    .setGuid(UUID.randomUUID().toString()); // generate new GUID
        }

        builder.setKind(kindEditText.getText().toString())
                .setValue(Double.parseDouble(transactionEditText.getText().toString()) * sign);

        Intent intent = new Intent();
        intent.putExtra(name, builder.build());
        intent.putExtra(EDITED_TRANSACTION_POSITION, getArguments().getInt(EDITED_TRANSACTION_POSITION));
        getActivity().setResult(resultCode, intent);
        getActivity().finish();
    }

    /**
     * Checks if input from user is correct. Value and kind of transaction can't be empty.
     *
     * @return true if input is correct, otherwise false
     */
    private boolean isInputCorrect() {
        if (kindEditText != null && TextUtils.isEmpty(kindEditText.getText())) {
            Toast.makeText(getActivity(), getResources().getString(R.string.kind_error), Toast.LENGTH_SHORT).show();
            return false;
        } else if (transactionEditText != null && TextUtils.isEmpty(transactionEditText.getText())) {
            Toast.makeText(getActivity(), getResources().getString(R.string.transaction_error), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return kindEditText != null && transactionEditText != null;
        }
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        changeTextView(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    /**
     * Changes text color to visualize different input depends if its income/outcome
     * and sets sign for resulting value to positive/negative.
     *
     * @param tabPosition the tab position that was selected
     */
    private void changeTextView(int tabPosition) {
        switch (tabPosition) {
            case INCOME:
                transactionEditText.setTextColor(getResources().getColor(R.color.green));
                sign = Math.abs(sign);
                break;
            case OUTCOME:
                transactionEditText.setTextColor(getResources().getColor(R.color.red));
                sign = Math.abs(sign) * (-1);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save state of currently selected ActionBar.Tab, transaction text value and kind text value
        outState.putInt(CURRENT_TAB_INT, ((ActionBarActivity) getActivity()).getSupportActionBar().getSelectedNavigationIndex());
        if (transactionEditText != null && transactionEditText.getText() != null)
            outState.putString(CURRENT_TRANSACTION_VALUE_STRING, transactionEditText.getText().toString());
        if (kindEditText != null && kindEditText.getText() != null)
            outState.putString(CURRENT_TRANSACTION_KIND_STRING, kindEditText.getText().toString());
    }
}

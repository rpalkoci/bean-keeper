package com.bean_keeper.utils;

import android.content.Context;

import com.bean_keeper.Proto;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Roman on 5/8/14.
 */

/**
 * Class for loading/storing {@code Proto.AccountDelta} data in/from file
 */
public class IOUtils {

    /**
     * Loads data from file
     *
     * @param context the context which the file is associated with
     * @param name the name of the file
     * @return the resulting {@code com.bean_keeper.Proto.AccountDelta} if the file exist, otherwise null
     */
    public static Proto.AccountDelta readDataFromFile(Context context, String name) {
        FileInputStream input = null;
        try {
            input = context.openFileInput(name);
            return Proto.AccountDelta.parseDelimitedFrom(input);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    /**
     * Stores data in in file
     *
     * @param context the context which the file is associated with
     * @param name the name of the file
     * @param accountDelta the data to store
     */
    public static void writeDataToFile(Context context, String name, Proto.AccountDelta accountDelta) {
        FileOutputStream output = null;
        try {
            output = context.openFileOutput(name, Context.MODE_PRIVATE);
            accountDelta.writeDelimitedTo(output);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
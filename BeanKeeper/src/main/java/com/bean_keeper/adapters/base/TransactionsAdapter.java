package com.bean_keeper.adapters.base;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bean_keeper.Proto;
import com.bean_keeper.R;
import com.bean_keeper.model.MyTransaction;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Roman on 5/8/14.
 */

/**
 * Adapter for all transaction
 */
public class TransactionsAdapter extends BaseAdapter {

    private static final String TAG = "TransactionsAdapter";
    /**
     * Pattern for {@code SimpleDateFormat} for displaying date of transaction
     */
    private static final String DATA_FORMAT = "EEEE, MMMM d, yyyy 'at' h:mm a";
    /**
     * List for holding deleted transaction
     */
    private List<Proto.Transaction> deletedTransactions = new ArrayList<Proto.Transaction>();
    /**
     * List for holding transaction to display to user
     */
    private List<MyTransaction> data = new ArrayList<MyTransaction>();

    private final Context context;
    private final LayoutInflater inflater;

    Calendar calendar;
    SimpleDateFormat format;
    /**
     * Total balance, sum all transactions
     */
    private double totalBalance;

    public double getTotalBalance() {
        return totalBalance;
    }

    public TransactionsAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        calendar = Calendar.getInstance();
        format = new SimpleDateFormat(DATA_FORMAT);
    }

    /**
     * Sets the data of the adapter
     *
     * @param newData the data of all transactions with flag dirty
     */
    public void setData(List<MyTransaction> newData) {
        this.data.clear();
        deletedTransactions.clear();
        totalBalance = 0;
        for (MyTransaction transaction : newData) {
            if (transaction.getTransaction().hasDeleted() && transaction.getTransaction().getDeleted()) {
                // deleted transactions are in adapter, not displaying to user
                deletedTransactions.add(transaction.getTransaction());
            } else {
                // data displayed to user
                this.data.add(transaction);
                totalBalance += transaction.getTransaction().getValue();
            }
        }
        Collections.sort(this.data);   // sort them by date
        notifyDataSetChanged();
    }

    /**
     * View holder for this adapter
     */
    static class ViewHolder {
        TextView kind, date, value;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null) {
            viewHolder = new ViewHolder();
            view = inflater.inflate(R.layout.item_transaction, null);
            viewHolder.kind = (TextView) view.findViewById(R.id.item_transaction_kind);
            viewHolder.date = (TextView) view.findViewById(R.id.item_transaction_date);
            viewHolder.value = (TextView) view.findViewById(R.id.item_transaction_value);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        Proto.Transaction item = data.get(i).getTransaction();
        viewHolder.kind.setText(item.getKind());
        calendar.setTimeInMillis(item.getDate());
        viewHolder.date.setText(format.format(calendar.getTime()));
        viewHolder.value.setText(String.valueOf(item.getValue()));

//        view.setBackgroundColor(data.get(i).isDirty() ? context.getResources().getColor(R.color.red) : context.getResources().getColor(R.color.green)); // TODO remove, only for show if the transaction is dirty or not

        return view;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    /**
     * Inserts new transaction in adapter added by user, so the flag dirty is set on true, at the top
     *
     * @param transaction new transaction to be added
     */
    public void addItem(Proto.Transaction transaction) {
        data.add(0, new MyTransaction(transaction, true));
        totalBalance += transaction.getValue();
        notifyDataSetChanged();
    }

    /**
     * Gets transactions from adapter by the given dirty flag
     *
     * @param dirty the dirty flag, true, if transaction to return has to be dirty(new or modified), false if data are
     *              synced with server
     * @return the list of transactions
     */
    public List<Proto.Transaction> getDataWithFlag(boolean dirty) {
        List<Proto.Transaction> result = new ArrayList<Proto.Transaction>();
        for (MyTransaction transaction : data) {
            if (transaction.isDirty() == dirty) {
                result.add(transaction.getTransaction());
            }
        }
        if (dirty) {
            // all deleted transaction are dirty
            result.addAll(deletedTransactions);
        }
        return result;
    }

    /**
     * Replace transaction at the specified  position with modified transaction
     *
     * @param position the position of transaction to replace
     * @param modifiedTransaction the modified transaction that replace old one
     */
    public void replaceAt(int position, Proto.Transaction modifiedTransaction) {
        try {
            totalBalance -= data.get(position).getTransaction().getValue();
            totalBalance += modifiedTransaction.getValue();
            data.get(position).setDirty(true);
            data.get(position).setTransaction(modifiedTransaction);
        } catch (IndexOutOfBoundsException e) {
            Log.e(TAG, "wrong position", e);
        }
        notifyDataSetChanged();
    }

    /**
     * Delete transaction at the specified position from adapter's data and adds it into
     * list of deleted transactions, so it is not visible for user
     *
     * @param position the position of transaction to delete
     */
    public void deleteItemAt(int position) {
        try {
            Proto.Transaction transaction = data.get(position).getTransaction();

            deletedTransactions.add(Proto.Transaction.newBuilder()  // Build transaction
                    .setGuid(transaction.getGuid())                 // with same data as previous transaction
                    .setKind(transaction.getKind())                 // at given position
                    .setValue(transaction.getValue())
                    .setDate(transaction.getDate())
                    .setDeleted(true)                               // except Deleted flag, this is sets to true
                    .build());
            totalBalance -= transaction.getValue();
            data.remove(position);
        } catch (IndexOutOfBoundsException e) {
            Log.e(TAG, "wrong position", e);
        }
        notifyDataSetChanged();
    }

    /**
     * Adds added and replace modified transactions after synchronization. And deletes all deleted transactions.
     *
     * @param addedOrModified the transaction that are new or modified after synchronization
     */
    public void updateData(List<Proto.Transaction> addedOrModified) {
        Map<String, MyTransaction> hashMap = new HashMap<String, MyTransaction>();
        // save all data in hash map and mark as clean
        for (MyTransaction myTransaction : data) {
            myTransaction.setDirty(false);
            hashMap.put(myTransaction.getTransaction().getGuid(), myTransaction);
        }
        List<MyTransaction> result = new ArrayList<MyTransaction>();

        for (Proto.Transaction transaction : addedOrModified) {
            hashMap.remove(transaction.getGuid());
            if (transaction.hasDeleted() && !transaction.getDeleted())
                result.add(new MyTransaction(transaction, false));
        }
        // add all transaction that left in hash map - was not deleted or modified
        result.addAll(hashMap.values());
        setData(result); // in this method deleted transactions are removed (deletedTransactions.clear()) and total balance is recalculate
    }
}

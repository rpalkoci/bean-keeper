package com.bean_keeper.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;

import com.bean_keeper.R;
import com.bean_keeper.fragments.AddOrEditTransactionFragment;
import com.bean_keeper.model.MyTransaction;

public class AddOrEditTransactionActivity extends ActionBarActivity {

    private static final String ADD_OR_EDIT_TRANSACTION_FRAGMENT_TAG = "addOrEditTransactionFragment";
    AddOrEditTransactionFragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_edit);

        FragmentManager fragmentManager = getSupportFragmentManager();
        mFragment = (AddOrEditTransactionFragment) fragmentManager.findFragmentByTag(ADD_OR_EDIT_TRANSACTION_FRAGMENT_TAG);

        if (mFragment == null) {
            mFragment = AddOrEditTransactionFragment
                    .newInstance(
                            getIntent().getIntExtra(AddOrEditTransactionFragment.EDITED_TRANSACTION_POSITION, -1),
                            (MyTransaction) getIntent().getSerializableExtra(AddOrEditTransactionFragment.EDITED_TRANSACTION));
            fragmentManager.beginTransaction()
                    .add(R.id.container, mFragment, ADD_OR_EDIT_TRANSACTION_FRAGMENT_TAG)
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_or_edit_transaction, menu);
        return true;
    }
}

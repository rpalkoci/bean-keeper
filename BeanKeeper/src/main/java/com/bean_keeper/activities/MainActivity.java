package com.bean_keeper.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;

import com.bean_keeper.R;
import com.bean_keeper.fragments.MainFragment;

public class MainActivity extends ActionBarActivity {

    public static final String MAIN_FRAGMENT_TAG = "main_fragment";
    MainFragment mainFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();
        mainFragment = (MainFragment) fragmentManager.findFragmentByTag(MAIN_FRAGMENT_TAG);

        if (mainFragment == null){
            mainFragment = new MainFragment();
            fragmentManager.beginTransaction()
                    .add(R.id.container, mainFragment, MAIN_FRAGMENT_TAG)
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

}
